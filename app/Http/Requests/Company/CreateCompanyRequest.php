<?php

namespace App\Http\Requests\Company;

use App\Models\Company;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Http\FormRequest;

class CreateCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'    => 'required|string|unique:companies,name',
            'email'   => 'required|string|email|max:255|unique:companies,email',
            'website' => 'nullable|url',
            'logo'    => 'image|dimensions:min_width=100,min_height=100'
        ];
    }

    public function storeCompany()
    {
        $this->uploadCompanyImage();

        $company = Company::create([
            'name'    => $this->name,
            'slug'    => str_slug($this->name),
            'email'   => $this->email,
            'website' => $this->website,
            'logo'    => $this->fileName
        ]);
    }

    public function uploadCompanyImage()
    {
        $uploadedImage = $this->logo;

        if($uploadedImage) {
            $this->fileName = basename(Storage::putFile('public/companies', new File($uploadedImage)));
        }

        return $this;
    }
}
