<?php

namespace App\Http\Requests\Department;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Http\FormRequest;

class UpdateDepartmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company'    => 'required|integer|exists:companies,id',
            'name'       => 'required|string',
        ];
    }

    public function updateDepartment($department)
    {
        $department->company_id = $this->company;
        $department->name       = $this->name;

        if($department->isDirty()) {
            $department->slug = str_slug($this->name);
            $department->save();
        }
    }
}
