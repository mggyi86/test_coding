<?php

namespace App\Http\Requests\Employee;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Http\FormRequest;

class UpdateEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company'    => 'required|integer|exists:companies,id',
            'department' => 'required|integer|exists:departments,id',
            'first_name' => 'required|string',
            'last_name'  => 'required|string',
            'email'      => 'required|string|email|max:255|unique:employees,email,' . $this->route('employee')->id,
            'phone'      => 'required|phone:MM'
        ];
    }

    public function updateEmployee($employee)
    {
        $employee->company_id    = $this->company;
        $employee->department_id = $this->department;
        $employee->first_name    = $this->first_name;
        $employee->last_name     = $this->last_name;
        $employee->email         = $this->email;
        $employee->phone         = $this->phone;

        if($employee->isDirty()) {
            $employee->save();
        }
    }
}
