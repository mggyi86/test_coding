<?php

namespace App\Http\Responses;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Contracts\Support\Responsable;

class ActivityLogsIndexResponse implements Responsable
{
    protected $activitylogs;

    public function __construct(Collection $activitylogs)
    {
        $this->activitylogs = $activitylogs;
    }

    public function toResponse($request)
    {
        if (request()->ajax()) {

            return DataTables::of($this->activitylogs)->addIndexColumn()
                    ->editColumn('causer', function($activitylog) {
                        return isset($activitylog->causer->name) ? $activitylog->causer->name : 'System';
                    })
                    ->editColumn('created_at', function($activitylog) {
                        return date('m-d-Y H:i:s A', strtotime($activitylog->created_at));
                    })
                    ->make(true);
        }

        return view('backend.activitylogs.index');
    }
}
