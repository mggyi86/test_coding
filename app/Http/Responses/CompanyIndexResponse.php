<?php

namespace App\Http\Responses;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Contracts\Support\Responsable;

class CompanyIndexResponse implements Responsable
{
    protected $companies;

    public function __construct(Collection $companies)
    {
        $this->companies = $companies;
    }

    public function toResponse($request)
    {
        if (request()->ajax()) {

            return DataTables::of($this->companies)->addIndexColumn()
                    ->editColumn('logo', function($company) {
                        return '<div class="thumbnail" style="max-height: 60px;overflow: hidden;">
                                <img alt="" src="'.$company->image_path.'"></div>';
                    })
                    ->editColumn('created_at', function($company) {
                        return date('m-d-Y H:i:s A', strtotime($company->created_at));
                    })
                    ->addColumn('action', function ($company) {
                        return '<a href="/backend/companies/'. $company->slug.'"
                                class="btn btn-sm btn-success"><i class="glyphicon glyphicon-eye-open"></i> </a>

                                <a href="/backend/companies/'.$company->slug.'/edit"
                                class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-edit"></i> </a>

                                <button data-remote="/backend/companies/'.$company->slug.'"
                                class="btn btn-sm btn-danger btn-delete"><i class="glyphicon glyphicon-trash"></i></button>';
                    })
                    ->rawColumns(['logo', 'action'])
                    ->make(true);
        }

        return view('backend.companies.index');
    }
}
