<?php

namespace App\Http\Responses;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Contracts\Support\Responsable;

class DepartmentIndexResponse implements Responsable
{
    protected $departments;

    public function __construct(Collection $departments)
    {
        $this->departments = $departments;
    }

    public function toResponse($request)
    {
        if (request()->ajax()) {

            return DataTables::of($this->departments)->addIndexColumn()
                    ->editColumn('company_id', function($department) {
                        return $department->company->name;
                    })
                    ->editColumn('created_at', function($department) {
                        return date('m-d-Y H:i:s A', strtotime($department->created_at));
                    })
                    ->addColumn('action', function ($department) {
                        return '<a href="/backend/departments/'. $department->slug.'"
                                class="btn btn-sm btn-success"><i class="glyphicon glyphicon-eye-open"></i> </a>

                                <a href="/backend/departments/'.$department->slug.'/edit"
                                class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-edit"></i> </a>

                                <button data-remote="/backend/departments/'.$department->slug.'"
                                class="btn btn-sm btn-danger btn-delete"><i class="glyphicon glyphicon-trash"></i></button>';
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }

        return view('backend.departments.index');
    }
}
