<?php

namespace App\Http\Responses;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Contracts\Support\Responsable;

class EmployeeIndexResponse implements Responsable
{
    protected $employees;

    public function __construct(Collection $employees)
    {
        $this->employees = $employees;
    }

    public function toResponse($request)
    {
        if (request()->ajax()) {

            return DataTables::of($this->employees)->addIndexColumn()
                    ->editColumn('company_id', function($employee) {
                        return $employee->company->name;
                    })
                    ->editColumn('department_id', function($employee) {
                        return $employee->department->name;
                    })
                    ->editColumn('created_at', function($employee) {
                        return date('m-d-Y H:i:s A', strtotime($employee->created_at));
                    })
                    ->addColumn('action', function ($employee) {
                        return '<a href="/backend/employees/'. $employee->uuid.'"
                                class="btn btn-sm btn-success"><i class="glyphicon glyphicon-eye-open"></i> </a>

                                <a href="/backend/employees/'.$employee->uuid.'/edit"
                                class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-edit"></i> </a>

                                <button data-remote="/backend/employees/'.$employee->uuid.'"
                                class="btn btn-sm btn-danger btn-delete"><i class="glyphicon glyphicon-trash"></i></button>';
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }

        return view('backend.employees.index');
    }
}
