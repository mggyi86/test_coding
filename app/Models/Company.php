<?php

namespace App\Models;

use App\Models\Employee;
use App\Models\Department;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Askedio\SoftCascade\Traits\SoftCascadeTrait;

class Company extends Model
{
    use SoftDeletes, SoftCascadeTrait, LogsActivity;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'companies';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug', 'email', 'logo', 'website'];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }

    public function departments()
    {
        return $this->hasMany(Department::class);
    }

    public function getImagePathAttribute()
    {
        return asset('storage/companies/' . $this->logo);
    }

    public function getDescriptionForEvent($eventName)
    {
        $reflect = new \ReflectionClass($this);
        return $reflect->getShortName() . " - {$this->name} has been {$eventName}.";
    }
}
