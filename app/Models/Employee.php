<?php

namespace App\Models;

use App\Models\Company;
use App\Models\Department;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Askedio\SoftCascade\Traits\SoftCascadeTrait;

class Employee extends Model
{
    use SoftDeletes, SoftCascadeTrait, LogsActivity;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'employees';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'uuid', 'email', 'phone', 'company_id', 'department_id'];

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function scopeUuid($query, $uuid)
    {
        return $query->where('id', $uuid);
    }

    public function getDescriptionForEvent($eventName)
    {
        $reflect = new \ReflectionClass($this);
        return $reflect->getShortName() . " - {$this->name} has been {$eventName}.";
    }
}
