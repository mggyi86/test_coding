<?php

namespace App\Models;

use App\Models\User;
use App\Models\Restaurant;
use App\Models\InitialPoint;
use App\Scopes\MerchantScope;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Askedio\SoftCascade\Traits\SoftCascadeTrait;

class Merchant extends User
{
    use LogsActivity;

    protected $softCascade = ['restaurants'];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new MerchantScope);
    }

    public function initial_point()
    {
        return $this->hasOne(InitialPoint::class);
    }

    public function restaurants()
    {
        return $this->hasMany(Restaurant::class);
    }

    public function getDescriptionForEvent($eventName)
    {
        $reflect = new \ReflectionClass($this);
        return $reflect->getShortName() . " - {$this->name} has been {$eventName}.";
    }
}
