<?php

namespace App\Models;

use App\Mode\Point;
use App\Models\Restaurant;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Askedio\SoftCascade\Traits\SoftCascadeTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable, HasRoles, SoftDeletes, LogsActivity, SoftCascadeTrait;

    protected $table = 'users';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'uuid', 'email', 'email_verified_at', 'password', 'phone', 'address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function point()
    {
        return $this->hasOne(Point::class);
    }

    public function scopeUuid($query, $uuid)
    {
        return $query->where('id', $uuid);
    }

    public function getDescriptionForEvent($eventName)
    {
        $reflect = new \ReflectionClass($this);
        return $reflect->getShortName() . " - {$this->name} has been {$eventName}.";
    }
}
