<?php

use App\Models\Company;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {
    $name = $faker->unique()->company;
    return [
        'name'    => $name,
        'slug'    => str_slug($name),
        'email'   => $faker->unique()->safeEmail,
        'logo'    => $faker->image($dir = public_path('storage/companies'), $width = 100, $height = 100, 'cats', false),
        'website' => $faker->unique()->url
    ];
});
