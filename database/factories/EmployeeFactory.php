<?php

use App\Models\Company;
use App\Models\Employee;
use App\Models\Department;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Employee::class, function (Faker $faker) {
    return [
        'first_name'    => $faker->name,
        'last_name'     => $faker->name,
        'uuid'          => Str::orderedUuid(),
        'email'         => $faker->unique()->safeEmail,
        'phone'         => $faker->e164PhoneNumber,
        'company_id'    => function() {
            return Company::all()->random();
        },
        'department_id' => function() {
            return Department::all()->random();
        }
    ];
});
