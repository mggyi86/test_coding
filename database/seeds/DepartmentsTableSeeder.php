<?php

use App\Models\Employee;
use App\Models\Department;
use Illuminate\Database\Seeder;
use App\Traits\TruncateTableSeeder;

class DepartmentsTableSeeder extends Seeder
{
    use TruncateTableSeeder;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncate('departments');

        factory(Department::class, 15)->create()->each(function($department) {
            // factory(Employee::class)->create([
            //     'department_id' => $department->id
            // ]);
            // for($i=0;$i<10;$i++) {
                $department->employees()->saveMany(factory(Employee::class, 10)->make());
            // }
        });
    }
}
