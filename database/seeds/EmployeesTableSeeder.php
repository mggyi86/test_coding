<?php

use App\Models\Employee;
use Illuminate\Database\Seeder;
use App\Traits\TruncateTableSeeder;

class EmployeesTableSeeder extends Seeder
{
    use TruncateTableSeeder;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncate('employees');

        factory(Employee::class, 50)->create();
    }
}
