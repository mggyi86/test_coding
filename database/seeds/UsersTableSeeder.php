<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use App\Traits\TruncateTableSeeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    use TruncateTableSeeder;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncate('users');
        $this->truncate('roles');
        $this->truncate('model_has_roles');

        $admin_role = Role::create(['name' => 'admin']);
        $merchant_role = Role::create(['name' => 'merchant']);

        //for super admin
        $admin = factory(User::class)->create([
                    'name' => 'Super Admin',
                    'email' => 'admin@admin.com',
                    'password' => Hash::make('password')
                 ]);
        $admin->assignRole($admin_role );

        //for merchant
        $merchant = factory(User::class)->create([
                        'name' => 'Merchant User',
                        'email' => 'merchant@merchant.com',
                        'password' => Hash::make('merchantmerchant')
                    ]);
        $merchant->assignRole($merchant_role );

        for($i=0; $i<30; $i++) {
            $merchant = factory(User::class)->create();
            $merchant->assignRole($merchant_role);
        }


        //for user
        factory(User::class, 60)->create();
    }
}
