@extends('layouts.master')

@section('title', 'Edit Department')

@push('css')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
@endpush

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('backend.dashboard') }}">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="{{ route('backend.departments.index') }}">Departments</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <span>Edit</span>
        </li>
    </ul>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> Department
    <small>management</small>
</h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="boxless tabbable-reversed">
            <div class="tab-content">
                <div class="tab-pane active" id="tab_0">
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-basket"></i>Edit Department </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"> </a>
                                <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                <a href="javascript:;" class="reload"> </a>
                                <a href="javascript:;" class="remove"> </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form action="{{ route('backend.departments.update', $department->slug) }}"
                            class="form-horizontal" method="POST" enctype="multipart/form-data">
                                {{ method_field('PUT') }}
                                @csrf
                                <div class="form-body">

                                    <div class="form-group{{ $errors->has('company') ? ' has-error ' : ''}}">
                                        <label for="company" class="col-md-3 control-label">Select Company</label>
                                        <div class="col-md-4">
                                            <select id="company" class="form-control select2" name="company" required>
                                                <option></option>
                                                @forelse($companies as $company)
                                                    <option value="{{ $company->id }}"
                                                    @if($company->name == $department->company->name)
                                                    selected="selected"
                                                    @endif
                                                    >
                                                        {{ $company->name }}
                                                    </option>
                                                @empty
                                                    Please Create Company First.
                                                @endforelse
                                            </select>
                                            @if ($errors->has('company'))
                                                <span class="help-block"> {{ $errors->first('company') }} </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('name') ? ' has-error ' : ''}}">
                                        <label for="name" class="col-md-3 control-label">First Name</label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" placeholder="First Name"
                                            name="name" value="{{ old('name') ? old('name') : $department->name }}" required>
                                            @if ($errors->has('name'))
                                                <span class="help-block"> {{ $errors->first('name') }} </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-4">
                                            <button type="submit" class="btn green">Submit</button>
                                            <a type="button" class="btn default" href="{{ route('backend.employees.index') }}">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<script>
$(document).ready(function() {
    $('#company').select2({
        placeholder: 'Select a Company',
        allowClear: true
    });
});
</script>
@endpush
