@extends('layouts.master')

@section('title', 'Create Employee')

@push('css')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
@endpush

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('backend.dashboard') }}">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="{{ route('backend.employees.index') }}">Employees</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <span>New</span>
        </li>
    </ul>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> Employee
    <small>management</small>
</h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="boxless tabbable-reversed">
            <div class="tab-content">
                <div class="tab-pane active" id="tab_0">
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-basket"></i>Create Employee </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"> </a>
                                <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                <a href="javascript:;" class="reload"> </a>
                                <a href="javascript:;" class="remove"> </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form action="{{ route('backend.employees.store') }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-body">

                                    <div class="form-group{{ $errors->has('company') ? ' has-error ' : ''}}">
                                        <label for="company" class="col-md-3 control-label">Select Company</label>
                                        <div class="col-md-4">
                                            <select id="company" class="form-control select2" name="company" required>
                                                <option></option>
                                                @forelse($companies as $company)
                                                    <option value="{{ $company->id }}">{{ $company->name }}</option>
                                                @empty
                                                    Please Create Company First.
                                                @endforelse
                                            </select>
                                            @if ($errors->has('company'))
                                                <span class="help-block"> {{ $errors->first('company') }} </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('department') ? ' has-error ' : ''}}">
                                        <label for="department" class="col-md-3 control-label">Select Department</label>
                                        <div class="col-md-4">
                                            <select id="department" class="form-control select2" name="department" required>
                                                <option></option>
                                                @forelse($departments as $department)
                                                    <option value="{{ $department->id }}">{{ $department->name }}</option>
                                                @empty
                                                    Please Create Department First.
                                                @endforelse
                                            </select>
                                            @if ($errors->has('department'))
                                                <span class="help-block"> {{ $errors->first('department') }} </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('first_name') ? ' has-error ' : ''}}">
                                        <label for="first_name" class="col-md-3 control-label">First Name</label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" placeholder="First Name"
                                            name="first_name" value="{{ old('first_name') }}" required>
                                            @if ($errors->has('first_name'))
                                                <span class="help-block"> {{ $errors->first('first_name') }} </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('last_name') ? ' has-error ' : ''}}">
                                        <label for="last_name" class="col-md-3 control-label">Last Name</label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" placeholder="Last Name"
                                            name="last_name" value="{{ old('last_name') }}" required>
                                            @if ($errors->has('last_name'))
                                                <span class="help-block"> {{ $errors->first('last_name') }} </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('email') ? ' has-error ' : ''}}">
                                        <label for="email" class="col-md-3 control-label">Email Address</label>
                                        <div class="col-md-4">
                                            <input type="email" class="form-control" placeholder="Email Address"
                                            name="email" value="{{ old('email') }}" required>
                                            @if ($errors->has('email'))
                                            <span class="help-block"> {{ $errors->first('email') }} </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('phone') ? ' has-error ' : ''}}">
                                        <label for="phone" class="col-md-3 control-label">Phone Number</label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" placeholder="Phone Number"
                                            name="phone" value="{{ old('phone') }}" required>
                                            @if ($errors->has('phone'))
                                                <span class="help-block"> {{ $errors->first('phone') }} </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-4">
                                            <button type="submit" class="btn green">Submit</button>
                                            <a type="button" class="btn default" href="{{ route('backend.employees.index') }}">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<script>
$(document).ready(function() {
    $('#company').select2({
        placeholder: 'Select a Company',
        allowClear: true
    });
    $('#department').select2({
        placeholder: 'Select a Department',
        allowClear: true
    });
});
</script>
@endpush
