@extends('layouts.master')

@section('title', 'Employee')

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('backend.dashboard') }}">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="{{ route('backend.employees.show', $employee->uuid) }}">Employee</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <span>Detail</span>
        </li>
    </ul>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> Employee
    <small>management</small>
</h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-basket"></i>Employee Detail </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                    <a href="{{ route('backend.employees.show', $employee->uuid) }}" class="reload"> </a>
                    <a href="javascript:;" class="remove"> </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th> Title </th>
                                <th> Description </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td> 1 </td>
                                <td> Company Name </td>
                                <td> {{ $employee->company->name }} </td>
                            </tr>
                            <tr>
                                <td> 2 </td>
                                <td> Department Name </td>
                                <td> {{ $employee->deparement->name }} </td>
                            </tr>
                            <tr>
                                <td> 3 </td>
                                <td> Name </td>
                                <td> {{ $employee->first_name }} </td>
                            </tr>
                            <tr>
                                <td> 4 </td>
                                <td> Contact Name </td>
                                <td> {{ $employee->last_name }} </td>
                            </tr>
                            <tr>
                                <td> 5 </td>
                                <td> Email </td>
                                <td> {{ $employee->email }} </td>
                            </tr>
                            <tr>
                                <td> 6 </td>
                                <td> Phone Number </td>
                                <td> {{ $employee->phone }} </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
@endsection
