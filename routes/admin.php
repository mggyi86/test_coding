<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "admin" middleware group. Enjoy building your Admin!
|
*/

Route::get('/', function () {
    return view('backend.dashboard');
});

Route::get('dashboard', function() {
    // dd(hexdec(uniqid()));
    return view('backend.dashboard');
})->name('dashboard');

Route::resource('companies', 'CompanyController');

Route::resource('departments', 'DepartmentController');

Route::resource('employees', 'EmployeeController');

Route::resource('activitylogs', 'ActivityLogsController')->only(['index']);
